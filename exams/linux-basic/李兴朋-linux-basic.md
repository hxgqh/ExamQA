# Linux管理测试
#### [作者] 胡小根
#### [邮箱] hxg@haomo-studio.com
#### [版本] v1.0.0
#### [标签]
* linux
* centos

## [选择题] 在正则表达式当中下面那一个字符集表示非空格字符?
#### [选项]
* A [:graph:]
* B [:digit:]
* C [:space:]
* D [:alpha:]

####[答案]
* A [:graph:]

## [选择题] 关于bash变量论述正确的是?
#### [选项]
* A 可以在/etc/porfile里面设置对所有用户生效，永久
* B 在用户家目录下的.bash_profile文件中添加变量对单一用户生效，临时生效
* C 可以使用export 定义，只对当前shell生效，永久有效
* D 以上说法都不对

####[答案]
* C 可以使用export 定义，只对当前shell生效，永久有效

## [选择题] 当登录linux后，登录的shell将查不同启动文件来处理其中的命令，处理文件的顺序是
#### [选项]
* A  /etc/profile --> ~/.bash_profile --> ~/.profile --> ~/.bash_login
* B  /etc/profile --> ~/.bash_profile --> ~/.bash_login --> ~/.profile
* A  ~/.bash_profile -->/etc/profile --> ~/.profile --> ~/.bash_login
* A  ~/.bash_login --> ~/.profile --> ~/.bash_profile  -->/etc/profile

####[答案]
* B  /etc/profile --> ~/.bash_profile --> ~/.bash_login --> ~/.profile

## [选择题] 创建多级目录命令是
#### [选项]
* A mkdir -p
* B mkdir -v
* C mkdir -m
* D mkdir -Z

####[答案]
* A mkdir -p

## [选择题] 有一些文件root用户也无法直接修改，可使用说明命令看看其权限
#### [选项]
* A ls -lh
* B file
* C stat
* D lsattr

####[答案]
* A ls -lh

## [选择题] linux上为了使用更多简洁操作，可以使用别名来简化命令，以下别名定义正确的是
#### [选项]
* A LS=‘ls -lh’
* B set  cnet ‘cd /etc/sysconfig/network-scripts/ifcfg-eth0’
* C alias die=‘rm -fr’
* D unalias die=‘rm -fr’

####[答案]
* C alias die=‘rm -fr’

## [选择题] 常见的文本编辑器工具有哪些
#### [选项]
* A nano
* B cat
* C cut
* D vim

####[答案]
* D vim
* A nano

## [选择题] 使用find命令查找文件时，以下哪个选项代表套接字文件
#### [选项]
* A d
* B l
* C p
* D s

####[答案]
* B l

## [选择题] 如一个文本文件的各权限位是这样的：-rw-r--r--，对该文件执行chmod g+s 后，其权限位应显示为
#### [选项]
* A -rw-r-Sr--
* B -rw-r-sr--
* C -rw-r--r--
* D -rw-r-xr--

####[答案]
* B -rw-r-sr--

## [选择题] 使用fdisk对磁盘进行分区时，LVM分区的类型为
#### [选项]
* A l
* B lvm
* C 9e
* D 8e

####[答案]
* A l

## [选择题] 使用tar命令对一个目录只打包，不压缩，应该使用如下哪条命令
#### [选项]
* A tar -cvf
* B tar -zcvf
* C tar -jvf
* D tar -jcvf

####[答案]
* A tar -cvf

## [选择题] shell脚本的哪个位置参数可以表示脚本本身
#### [选项]
* A $0
* B $1
* C $#
* D $@

####[答案]
* A tar -cvf

## [选择题] 通常我常说的四层负载和七层负载对应OSI模型分别是?
#### [选项]
* A 传输层,表示层
* B 网络层,应用层
* C 传输层,应用层
* D 网络层,表示层

####[答案]
* A tar -cvf

## [选择题] 系统中存在两个进程, 其pid分别为 110, 119, 此时希望当119需要占用CPU时总是要优于110, 应如何做？
#### [选项]
* A 调整进程119的nice值, nice -5 119
* B 调整进程119的nice值, renice -5 119
* C 调整进程110的nice值, nice -5 110
* D 调整进程119的nice值, renice -5 110


####[答案]
* C 调整进程110的nice值, nice -5 110

## [选择题] 如何在非交互式的情况下为用户magedu添加一条crontab任务？
#### [选项]
* A 将任务写入到/var/spool/cron/root
* B 将任务写入到/var/spool/cron/magedu
* C 将任务写入到/var/spool/root/cron
* D 将任务写入到/var/spool/magedu/cron

## [选择题] CentOS7系统中启动网络服务的命令是：
#### [选项]
* A service network start
* B /etc/init.d/network start
* C systemctl start network.service
* D /etc/init.d/rc.d/init.d start

## [选择题] kernel实现自身初始化时，实现的任务为
#### [选项]
* A 探测可识别的所有硬件；
* B 以只读方式装载“真正根文件系统”
* C 按次序查找各引导设备，第一个有引导程序的设备即为启动系统用到的设备
* D 启动用户空间的第一个应用程序：/sbin/init

## [选择题] CentOS 6系统中，Linux系统运行级别，关系对应不正确的是
#### [选项]
* A 4--维护模式，多用户模式，需要用户认让，会启动网络功能，但不支持使用NFS；
* B 1--维护模式，单用户模式，sigle模式；此级别可允许root用户直接登录而无须认证；
* C 3--正常级别，完全多用户模式，文本界面；
* D 5--正常级别，完全多用户模式，图形界面；

## [选择题] 下面命令，可以输出/etc/passwd文件中root用户所在行的是
#### [选项]
* A awk '{if($1=/root/) print }' /etc/passwd
* B awk -F: '{if($1==/root/) print }' /etc/passwd
* C awk -F: '{if($1=/root/) print }' /etc/passwd
* D awk -F: '{if($1~/root/) print }' /etc/passwd

## [选择题] 以下不是/ect/rc.d/rc.sysinit的功能的是
#### [选项]
* A 根据配置文件中的设定来设定主机名
* B 配置服务器ip地址
* C 打印文本欢迎信息
* D 激活LVM和RAID设备

## [选择题]  下面的网络协议中，面向连接的的协议是
#### [选项]
* A 传输控制协议
* B 用户数据报协议
* C 网际协议
* D 网际控制报文协议

## [选择题]  Linux文件权限一共10位长度，分成四段，第三段表示的内容是
#### [选项]
* A 文件类型
* B 文件所有者的权限
* C 文件所有者所在组的权限
* D 其他用户的权限

####[答案]
* C 文件所有者所在组的权限

## [选择题]  终止一个前台进程可能用到的命令和操作
#### [选项]
* A kill
* B <CTRL>+C
* C shut down
* D halt

####[答案]
* B <CTRL>+C

## [选择题] 在使用mkdir命令创建新的目录时，在其父目录不存在时先创建父目录的选项是
#### [选项]
* A -m
* B -d
* C -f
* D -p

####[答案]
* D -p

## [选择题]  具有很多C语言的功能，又称过滤器的是
#### [选项]
* A csh
* B tcsh
* C awk
* D sed

####[答案]

## [选择题]  一台主机要实现通过局域网与另一个局域网通信，需要做的工作是
#### [选项]
* A 配置域名服务器
* B 定义一条本机指向所在网络的路由
* C 定义一条本机指向所在网络网关的路由
* D 定义一条本机指向目标网络网关的路由

## [选择题]  局域网的网络地址192.168.1.0/24，局域网络连接其它网络的网关地址是192.168.1.1。主机192.168.1.20访问172.16.1.0/24网络时，其路由设置正确的是
#### [选项]
* A route add –net 192.168.1.0 gw 192.168.1.1 netmask 255.255.255.0 metric 1
* B route add –net 172.16.1.0 gw 192.168.1.1 netmask 255.255.255.255 metric 1
* C route add –net 172.16.1.0 gw 172.16.1.1 netmask 255.255.255.0 metric 1
* D route add default 192.168.1.0 netmask 172.168.1.1 metric 1

## [选择题]  下列提法中，不属于ifconfig命令作用范围的是
#### [选项]
* A 配置本地回环地址
* B 配置网卡的IP地址
* C 激活网络适配器
* D 加载网卡到内核中

####[答案]
* D 加载网卡到内核中

## [选择题]  下列关于链接描述，错误的是
#### [选项]
* A 硬链接就是让链接文件的i节点号指向被链接文件的i节点
* B 硬链接和符号连接都是产生一个新的i节点
* C 链接分为硬链接和符号链接
* D 硬连接不能链接目录文件

## [选择题]  在局域网络内的某台主机用ping命令测试网络连接时发现网络内部的主机都可以连同，而不能与公网连通，问题可能是
#### [选项]
* A 主机IP设置有误
* B 没有设置连接局域网的网关
* C 局域网的网关或主机的网关设置有误
* D 局域网DNS服务器设置有误

## [选择题]  下列文件中，包含了主机名到IP地址的映射关系的文件是：
#### [选项]
* A /etc/HOSTNAME
* B /etc/hosts
* C /etc/resolv.conf
* D /etc/networks

## [选择题]  ___命令可以从文本文件的每一行中截取指定内容的数据。
#### [选项]
* A cp
* B dd
* C fmt
* D cut

####[答案]
* B dd

## [选择题]  若一台计算机的内存为128MB，则交换分区的大小通常是
#### [选项]
* A 64MB
* B 128MB
* C 256MB
* D 512MB

## [选择题]  Linux有三个查看文件的命令，若希望在查看文件内容过程中可以用光标上下移动来查看文件内容，应使用____命令。
#### [选项]
* A cat
* B more
* C less
* D menu

####[答案]
* B more

## [选择题] 在TCP/IP模型中，应用层包含了所有的高层协议，在下列的一些应用协议中，___是能够实现本地与远程主机之间的文件传输工作。
#### [选项]
* A telnet
* B FTP
* C SNMP
* D NFS

####[答案]
* B FTP

## [选择题] 当我们与某远程网络连接不上时，就需要跟踪路由查看，以便了解在网络的什么位置出现了问题，满足该目的的命令是
#### [选项]
* A ping
* B ifconfig
* C traceroute
* D netstat

## [选择题] 对名为fido的文件用chmod 551 fido 进行了修改，则它的许可权是
#### [选项]
* A -rwxr-xr-x
* B -rwxr--r--
* C -r--r--r--
* D -r-xr-x—x

####[答案]
* D -r-xr-x—x

## [选择题]  用ls –al 命令列出下面的文件列表，___文件是符号连接文件。
#### [选项]
* A -rw-rw-rw- 2 hel-s users 56 Sep 09 11:05 hello
* B -rwxrwxrwx 2 hel-s users 56 Sep 09 11:05 goodbey
* C drwxr--r-- 1 hel users 1024 Sep 10 08:10 zhang
* D lrwxr--r-- 1 hel users 2024 Sep 12 08:12 cheng

####[答案]
* D lrwxr--r-- 1 hel users 2024 Sep 12 08:12 cheng

## [选择题] DNS域名系统主要负责主机名和___之间的解析。
#### [选项]
* A IP地址
* B MAC地址
* C 网络地址
* D 主机别名

####[答案]
* C 网络地址

## [选择题]  WWW服务器是在Internet上使用最为广泛，它采用的是___结构。
#### [选项]
* A 服务器/工作站
* B B/S
* C 集中式
* D 分布式

## [选择题] Linux系统通过___命令给其他用户发消息。
#### [选项]
* A less
* B mesg y
* C write
* D echo to

####[答案]
* D echo to

## [选择题] Linux文件系统的文件都按其作用分门别类地放在相关的目录中，对于外部设备文件，一般应将其放在___目录中。
#### [选项]
* A /bin
* B /etc
* C /dev
* D /lib

####[答案]
* C /dev

## [选择题] 在重新启动Linux系统的同时把内存中的信息写入硬盘，应使用___命令实现。
#### [选项]
* A # reboot
* B # halt
* C # reboot
* D # shutdown –r now

## [选择题] 关闭linux系统（不重新启动）可使用命令
#### [选项]
* A Ctrl+Alt+Del
* B halt
* C shutdown -r now
* D reboot

####[答案]
* C shutdown -r now

## [选择题] 实现从IP地址到以太网MAC地址转换的命令为
#### [选项]
* A ping
* B ifconfig
* C arp
* D traceroute

####[答案]
* D traceroute

## [选择题] 在vi编辑器中的命令模式下，键入___可在光标当前所在行下添加一新行。
#### [选项]
* A <a>;
* B <o>;
* C <I>;
* D A

####[答案]
* B <o>;

## [选择题] 在vi编辑器中的命令模式下，删除当前光标处的字符使用___命令。
#### [选项]
* A <x>;
* B <d><w>;
* C <D>;
* D <d><d>;

####[答案]
* A <x>;

## [选择题] 在vi编辑器中的命令模式下，重复上一次对编辑的文本进行的操作，可使用___命令。
#### [选项]
* A 上箭头
* B 下箭头
* C <.>;
* D <*>

####[答案]
* B 下箭头

## [选择题] 用命令ls -al显示出文件ff的描述如下所示，由此可知文件ff的类型为
	-rwxr-xr-- 1 root root 599 Cec 10 17:12 ff
#### [选项]
* A 普通文件
* B 硬链接
* C 目录
* D 符号链接

####[答案]
* A 普通文件

## [选择题] 删除文件命令为
#### [选项]
* A mkdir
* B rmdir
* C mv
* D rm

####[答案]
* B rmdir
* D rm

## [选择题] 网络管理员对WWW服务器进行访问、控制存取和运行等控制，这些控制可在___文件中体现。
#### [选项]
* A httpd.conf
* B lilo.conf
* C inetd.conf
* D resolv.conf

####[答案]
* A httpd.conf

## [选择题] DHCP是动态主机配置协议的简称，其作用是可以使网络管理员通过一台服务器来管理一个网络系统，自动地为一个网络中的主机分配____地址。
#### [选项]
* A 网络
* B MAC
* C TCP
* D IP

####[答案]
* D IP

## [选择题] 对文件进行归档的命令为
#### [选项]
* A dd
* B cpio
* C gzip
* D tar

####[答案]
* D tar

## [选择题] 改变文件所有者的命令为
#### [选项]
* A chmod
* B touch
* C chown
* D cat

####[答案]
* C chown

## [选择题] 在给定文件中查找与设定条件相符字符串的命令为
#### [选项]
* A grep
* B gzip
* C find
* D sort

####[答案]
* C find

## [选择题] 建立一个新文件可以使用的命令为
#### [选项]
* A chmod
* B more
* C cp
* D touch

####[答案]
* D touch

## [选择题] 在下列命令中，不能显示文本文件内容的命令是
#### [选项]
* A more
* B less
* C tail
* D join

####[答案]
* D join

## [选择题] i节点是一个___长的表，表中包含了文件的相关信息。
#### [选项]
* A 8字节
* B 16字节
* C 32字节
* D 64字节

####[答案]
* D 64字节

## [选择题] 文件权限读、写、执行的三种标志符号依次是
#### [选项]
* A rwx
* B xrw
* C rdx
* D srw

####[答案]
* A rwx

## [选择题] Linux 文件名的长度不得超过___个字符。
#### [选项]
* A 64
* B 128
* C 256
* D 512

####[答案]
* B 256

## [选择题] 进程有三种状态
#### [选项]
* A 准备态、执行态和退出态
* B 精确态、模糊态和随机态
* C 运行态、就绪态和等待态
* D 手工态、自动态和自由态

####[答案]
* C 运行态、就绪态和等待态

## [选择题]  从后台启动进程，应在命令的结尾加上符号
#### [选项]
* A &
* B @
* C #
* D $

####[答案]
* B @

## [选择题] 在Shell脚本中，用来读取文件内各个域的内容并将其赋值给Shell变量的命令是
#### [选项]
* A fold
* B join
* C tr
* D read

####[答案]
* D read

## [选择题] crontab文件由六个域组成，每个域之间用空格分割，其排列如下
#### [选项]
* A MIN HOUR DAY MONTH YEAR COMMAND
* B MIN HOUR DAY MONTH DAYOFWEEK COMMAND
* C COMMAND HOUR DAY MONTH DAYOFWEEK
* D COMMAND YEAR MONTH DAY HOUR MIN

## [选择题] 某文件的组外成员的权限为只读；所有者有全部权限；组内的权限为读与写，则该文件的权限为
#### [选项]
* A 467
* B 674
* C 476
* D 764

####[答案]
* D 764

## [选择题] Apache服务器默认的接听连接端口号是
#### [选项]
* A 1024
* B 800
* C 80 (http)
* D 8

####[答案]
* C 80 (http)

## [选择题] OpenSSL是一个
#### [选项]
* A 加密软件
* B 邮件系统
* C 数据库管理系统
* D 嵌入式脚本编程语言

####[答案]
* A 加密软件

## [选择题] 退出交互模式的shell，应键入
#### [选项]
* A <Esc>;
* B ^q
* C exit
* D quit

####[答案]
* D quit

## [选择题] 设超级用户root当前所在目录为：/usr/local，键入cd命令后，用户当前所在目录为
#### [选项]
* A /home
* B /root
* C /home/root
* D /usr/local

####[答案]
* C /home/root

## [选择题] 将光盘CD-ROM（hdc）安装到文件系统的/mnt/cdrom目录下的命令是
#### [选项]
* A mount /mnt/cdrom
* B mount /mnt/cdrom /dev/hdc
* C mount /dev/hdc /mnt/cdrom
* D mount /dev/hdc

####[答案]
* C mount /dev/hdc /mnt/cdrom

## [选择题] 将光盘/dev/hdc卸载的命令是
#### [选项]
* A umount /dev/hdc
* B unmount /dev/hdc
* C umount /mnt/cdrom /dev/hdc
* D unmount /mnt/cdrom /dev/hdc

####[答案]
* A umount /dev/hdc

## [选择题] 在DNS配置文件中，用于表示某主机别名的是
#### [选项]
* A NS
* B CNAME
* C NAME
* D CN

## [选择题] 可以完成主机名与IP地址的正向解析和反向解析任务的命令是
#### [选项]
* A nslookup
* B arp
* C ifconfig
* D dnslook

## [选择题] 已知某用户stud1，其用户目录为/home/stud1。分页显示当前目录下的所有文件的文件或目录名、用户组、用户、文件大小、文件或目录权限、文件创建时间等信息的命令是
#### [选项]
* A more ls –al
* B more –al ls
* C more < ls –al
* D ls –al | more

####[答案]
* A more ls –al

## [选择题] 系统中有用户user1和user2，同属于users组。在user1用户目录下有一文件file1，它拥有644的权限，如果user2用户想修改user1用户目录下的file1文件，应拥有___权限。
#### [选项]
* A 744
* B 664
* C 646
* D 746

####[答案]
* B 664

## [选择题] ___命令是在vi编辑器中执行存盘退出。
#### [选项]
* A :q
* B ZZ
* C :q!
* D :WQ

####[答案]
* D :WQ

## [选择题] ___设备是字符设备。
#### [选项]
* A hdc
* B fd0
* C hda1
* D tty1

## [选择题] ___目录存放着Linux的源代码。
#### [选项]
* A /etc
* B /usr/src
* C /usr
* D /home

## [选择题] ___不是进程和程序的区别。
#### [选项]
* A 程序是一组有序的静态指令，进程是一次程序的执行过程
* B 程序只能在前台运行，而进程可以在前台或后台运行
* C 程序可以长期保存，进程是暂时的
* D 程序没有状态，而进程是有状态的

## [选择题] 文件exer1的访问权限为rw-r--r--，现要增加所有用户的执行权限和同组用户的写权限，下列命令正确的是
#### [选项]
* A chmod a+x g+w exer1
* B chmod 765 exer1
* C chmod o+x exer1
* D chmod g+w exer1

## [选择题] 有关归档和压缩命令，下面描述正确的是
#### [选项]
* A 用uncompress命令解压缩由compress命令生成的后缀为.zip的压缩文件
* B unzip命令和gzip命令可以解压缩相同类型的文件
* C tar归档且压缩的文件可以由gzip命令解压缩
* D tar命令归档后的文件也是一种压缩文件

## [选择题] 不是shell具有的功能和特点的是
#### [选项]
* A 管道
* B 输入输出重定向
* C 执行后台进程
* D 处理程序命令

## [命令题] 找出当前目录下10天没有改变，大小大于4K的普通文件或目录

####[答案]
$ find / -size +4k -mtime -10;


## [命令题] 使用ss命令， 查看当前系统上处于time_wait 状态的连接

## [命令题] 在每周二的凌晨1点5分执行脚本/data/get_username.sh，并将脚本的输出写至/tmp/get_username.log日志文件中

## [命令题] 查看当前系统的版本号。请用一行命令完成操作。

####[答案]
$ uname -r

## [命令题] 给系统外接新硬盘，并挂载到/mount/bak目录。

## [命令题] 查看系统CPU/Mem使用情况。请用一行命令完成操作。

## [命令题] 查看接口当前的上传、下载速度。请用一行命令完成操作。

## [命令题] 杀死正在运行中的程序。请用一行命令完成操作。

####[答案]
$ kill

## [命令题] 查看指令top的手册。请用一行命令完成操作。

## [命令题] 创建用户exam，HOME目录为/home/exam，并使其具有sudo权限。请用一行命令完成操作。

## [命令题] 创建目录/home/exam/exam，并将其设置为当前用户可读可写可执行、当前组只可执行、其他组不可读不可写不可执行。请用一行命令完成操作。

####[答案]
$ mddir -mp 710 /home/exam/exam;

## [命令题] 找出系统内所有大于10Mb的文件。请用一行命令完成操作。

####[答案]
$ find -size +10000k

## [命令题] 对于已有的文件/home/exam/exam，其包含一行为"Hello World"，请在不用编辑器打开文件的情况下，用linux命令，修改为"Hello Exam"。请用一行命令完成操作。

## [命令题] 对于已有的文件/home/exam/exam，其包含一行为"Hello World"，请用vim编辑器打开文件，不进入编辑模式，修改为"Hello Exam"。请输入命令。

####[答案]
$:  vim ／home/exam/exam 1,$s/"Hello World"/"Hello Exam"/g

## [命令题] vim编辑器内，如何设置显示行号。请输入命令。

####[答案]
$ set nu

## [命令题] 请用一行命令，只列出系统所有进程包含"grep"的所有进程的进程号。请用一行命令完成操作。

## [命令题] 设置自己的电脑，使得今后可以简便的方式用ssh进入haomo-studio.com。进入的命令为ssh root@haomo。 请用一行命令完成操作。

## [命令题] 开启系统的ssh服务，使得物理机可以无密码登录虚拟机。

## [命令题] 修改yum的源，增加163的源。163源见文件[CentOS7-Base-163.repo](./files/CentOS7-Base-163.repo)


